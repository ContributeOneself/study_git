#study_git

### 生成并部署SSH key
1.如何生成ssh公钥
你可以按如下命令来生成 sshkey : 
```shell
    $ ssh-keygen -t rsa -C "xxxxx@xxxxx.com"  #你的邮箱
    # 然后输入你要保存的  rsa 名字
    # 2次密码
```

2、查看你的 public key，并把他添加到码云（Gitee.com） SSH key添加地址
```shell
    $ cat ~/.ssh/id_rsa.pub
    # ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC6eNtGpNGwstc....

    # 添加后，在终端（Terminal）中输入: 验证你生成的是否可以oschina
    $ ssh -T git@gitee.com

    # 你也可以联想到github
    # ssh -T git@git.github.com

    # 若返回

    Welcome to Git@OSC, yourname!
    #则证明添加成功。
```


3、还是不成功
```shell
 $ ssh -T git@gitee.com                                
Permission denied (publickey).
```
没关系，你先生成，一定要记住你刚刚 输入2次密码的那个密码，然后用sourceTree进行`克隆(clone)`，你会一步成功了。

你在 新增、修改 文件后，想提交(push)，你会发现，不可以了，需要输入`用户名`和`密码`，就是 你的`邮箱`，和你输入的2次密码的那个`密码`。
确定后，你就提交成功了。

```shell
 # 如果`码云`也像 `GitHub` 弄个客户端，就不会有这些问题了。前提你是用 `GitHub客户端(GitHub Desktop)` 
```

---

### git 操作
---

```sh
    mkdir nodejs-cli-takeaway
    cd nodejs-cli-takeaway
    git init
    touch README.md
    git add README.md
    git commit -m "first commit"
    git remote add origin git@gitee.com:srxboys/nodejs-cli-takeaway.git
    git push -u origin master
```

---
branch操作
创建分支
```shell
   $ git branch branch_name
```

 切换分支：
```shell
   $ git checkout branch_name
```

 创建并切换分支：
```shell
   $ git checkout -b branch_name
```

 更新master主线上的东西到该分支上：
```shell
   $ git rebase master
```

 切换到master分支：
```shell
   $ git checkout master
```

 更新branch_name 分支上的东西到master上：
```shell
   $ git rebase branch_name
```

 提交：
```shell
   $ git commit -a
```

 对最近一次commit的进行修改：
```shell
   $ git commit -a –amend  #? 没有实践过
```

 commit之后，如果想撤销最近一次提交(即退回到上一次版本)并本地保留代码：
```shell
   $ git reset HEAD^ #?
```

 合并分支：(merge from)
```shell
   $ git checkout master
   $ git merge branch_name (merge from branch_name )  #?
```

 删除分支：
```shell
   $ git branch -d branch_name ... branch_name
```

 强制删除分支：
```shell
   $ git branch -D branch_name
```

 列出所有分支：
```shell
   $ git branch
```

 查看各个分支最后一次提交：
```shell
   $ git branch -v  #？
```

 查看哪些分支合并入当前分支：
```shell
   $ git branch --merged #?
```

 查看哪些分支未合并入当前分支：
```shell
   $ git branch –no-merged  #?
```

 更新远程库到本地：
```shell
   $ git fetch origin  #?
```

 推送分支：
```shell
   $ git push origin branch_name
```

 取远程分支合并到本地：
```shell
   $ git merge origin/branch_name  #?
```

 取远程分支并分化一个新分支：
```shell
   $ git checkout -b branch_name origin/branch_name  #?
```

 删除远程分支：
```shell
   $ git push origin :branch_name #?
```

 rebase:
```shell
   $ git checkout branch_name
   $ git rebase master (rebase from master)  #?
```

---

 tag操作
 强制替换tag 并删除old tag
```shell
   # git tag -f  <new tag  <old tag
   $ git tag -f v2.4.0 2.4.0
```

 普通删除tag
```shell
   # git tag -d <tag name
   $ git tag -d v2.4.0
```


 切换到某tag上，完善功能.
 1、切换(创建并切换)到某个tag(v0.1.0) 并在 test(分支) 上开发(修改本地，可以 以新分支去提交)
```shell
   $ git checkout -b tset v0.1.0
```

 2、切换到tag历史记录会处在分离头指针状态，这个是的修改是很危险的(只能修改本地，不能提交) 
```shell
   $ git checkout v0.1.0
```

---
