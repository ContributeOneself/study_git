```sh
 # 分支使用
 master # 最好不能被commit push 操作，只允许merge。
 dev # 是开发分支，尽量检验好在提交commit
-------------------------------------
 dev # 在版本迭代结束已经发版后，才可 ①create tag  ② merge dev to master


--------------------------------------------------------------------------
 # 使用 git
 
 # 克隆
   git clone -b <分支名>  <远程库http地址>    # 第一次、第一人 下载代码、本地库

   # 下面简易方法
   #    1)进入，要放项目的目录。克隆命令 git clone
   git clone <远程库http地址> # 克隆的分支是`远程库`默认分支(并带有所有分支记录)

   #    2)sourceTree 添加一下本地库


# 日志
   git log
   git log | more

# 状态
   git status  # (不推荐) git st




 # 查看分支
   git branch  # (不推荐) git br
   git branch -a    # 查看所有本地分支和远程分支
   git branch -r    # 只查看远程分支

 # 切换分支
   git checkout <分支名>

 # 创建分支
   git branch <分支名>

 # 创建分支并切换到新创建的分支
   git checkout -b  <分支名>

 # 删除分支
   git branch -D <分支名>    # 本地分支且没有被合并(merge)
   git branch -d <分支名>    # 删除完全合并(merge)的分支
 


# 添加文件(添加到本地缓存区)
    git add . # 他会监控工作区的状态树，使用它会把工作时的所有变化提交到暂存区，包括文件内容修改(modified)以及新文件(new)，但不包括被删除的文件。

    git add -u # 他仅监控已经被add的文件（即tracked file），他会将被修改的文件提交到暂存区。add -u 不会提交新文件（untracked file）。（git add --update的缩写）

    git add -A # 是上面两个功能的合集（git add --all的缩写）

 # 提交commit 到本地库 (只提交本地缓存区的内容)
   git commit -m "注释"   # (不推荐) git ci -m "注释"

 # 添加文件 + 提交commit 到本地库 (提交 缓存区和没有添加到缓存区的内容)
   git commit -a -m "注释"
 


 # 提交push操作(已下 `master` 为例子而已，如果使用按实际情况)
   git push  # (不推荐)如果当前分支只有一个远程分支，那么主机名都可以省略
   git push --all origin # (危险操作)当遇到这种情况就是不管是否存在对应的远程分支，将本地的所有分支都推送到远程主机，这时需要 -all 选项  
   git push --force origin git push # (危险操作)如果需要本地先git pull更新到跟服务器版本一致，如果本地版本库比远程服务器上的低，那么一般会提示你git pull更新，如果一定要提交，那么可以使用这个命令
   git push origin --tags # 不会推送分支，如果一定要推送标签的话那么可以使用这个命令
   git push origin # 如果当前分支与远程分支存在追踪关系，则本地分支和远程分支都可以省略，将当前分支推送到origin主机的对应分支 
   git push origin master # 如果远程分支被省略，如上则表示将本地分支推送到与之存在追踪关系的远程分支（通常两者同名），如果该远程分支不存在，则会被新建
   git push origin ：refs/for/master # 如果省略本地分支名，则表示删除指定的远程分支，因为这等同于推送一个空的本地分支到远程分支，等同于 git push origin --delete master
   git push -u origin master # (不推荐)如果当前分支与多个主机存在追踪关系，则可以使用 -u 参数指定一个默认主机，这样后面就可以不加任何参数，等同于使用git push
   git push -u origin master -f # (危险操作) 注释和上面一样，强调：“提交本地代码强制提交到远程，如果远程高于本地记录标号，那么远程将被本地覆盖”



 # 拉取代码
   git pull   # (不推荐) git pl
   git fetch origin


  # 创建轻量级标签实际上就是一个保存着对应提交对象的校验和信息的文件。要创建这样的标签，一个 -a，-s 或 -m 选项都不用，直接给出标签名字即可
   git tag v3.22.0

 # 创建tag(标签)
   git tag -a v3.22.0 -m "3.22.0版本的tag"  # 只是创建本地库tag

 # 删除tag(标签)
   git tag -d v3.22.0 # 只是删除本地库tag

 # 补打tag(标签)
   git tag -a v3.22.0 9fbc3d0  # 只是创建本地库tag

 # 将本地库的tag推送到origin(远程库)
   git push origin v3.22.0 # 将v3.22.0标签提交到git服务器
   git push origin –tags # 将本地所有标签一次性提交到git服务器

--------------------------------------------------------------------------
  
 # 克隆 某个远程分支
   git clone -b <分支名> <git地址>

 # 某个远程tag 创建一个新的分支(前提是有远程、本地库在操作)
   git checkout -b <新的分支名> <tag名>  

 # 某个远程分支的代码 并创建新的分支在本地库中(前提是有远程、本地库在操作)
   git checkout -b <新的分支名> --track origin/Dev 


```